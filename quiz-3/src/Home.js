import React, { useState, useEffect } from 'react';

const Home = () => {
  // State to store the fetched game data
  const [gameData, setGameData] = useState([]);

  // Fetch data from the API
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://backendexample.sanbercloud.com/api/mobile-apps');
        const data = await response.json();
        setGameData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <section className="bg-gray-200 p-5">
         <div className="container mx-auto mt-10">
            <h1 className="text-xl font-bold ">Find your data that you need!</h1>
          </div>

        <div className="container mx-auto flex-wrap flex gap-10 items-center justify-start">
            <div className="container mx-auto flex-wrap flex gap-10 items-center justify-start">
                {/* Map through the gameData array and render individual cards */}
                {gameData.map((game) => (
                    <div key={game.id} className="mt-10 h-72 flex max-w-xl bg-white shadow-lg rounded-lg overflow-hidden">
                    <img
                        src={game.image_url}
                        alt={game.name}
                        className="w-1/3 bg-cover bg-center bg-landscape"
                        onError={(e) => {
                        // Handle image loading errors
                        e.target.src = 'fallback-image-url.jpg'; // Replace with a fallback image URL
                        }}
                    />
                    <div className="w-2/3 p-4">
                        <h1 className="text-gray-900 font-bold text-2xl">{game.name}</h1>
                        <small>{game.release_year}</small>
                        <p className="mt-2 text-gray-600 text-sm">{game.description}</p>
                        <div className="item-center mt-2 text-gray-500">
                        <span>{game.category}</span>
                        <div className="flex items-center justify-between mt-3">
                            <h1 className="text-gray-700 font-bold text-xl">
                            {game.price === 0 ? "FREE" : `Rp ${game.price}`}
                            </h1>
                            <button className="px-3 py-2 bg-gray-800 text-white text-xs font-bold uppercase rounded">
                            {game.rating} Ratings
                            </button>
                        </div>
                        </div>
                    </div>
                    </div>
                ))}
            </div>
        </div>  
    </section>
    
  );
};

export default Home;
