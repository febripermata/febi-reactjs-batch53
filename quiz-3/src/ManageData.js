import React, { useState, useEffect } from 'react';

const ManageData = () => {
  const [mobileApps, setMobileApps] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://backendexample.sanbercloud.com/api/mobile-apps');
        const data = await response.json();
        setMobileApps(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const handleUpdateData = async (id) => {
    try {
      console.log(`Updating data for ID: ${id}`);
  
      // Fetch existing data
      const existingDataResponse = await fetch(`https://backendexample.sanbercloud.com/api/mobile-apps/${id}`);
      const existingData = await existingDataResponse.json();
  
      console.log('Existing Data:', existingData);
  
      // Make changes to the existing data (replace these values as needed)
      const updatedData = {
        ...existingData,
        name: 'Updated App Name',
        description: 'Updated App Description',
      };
  
      console.log('Updated Data:', updatedData);
  
      // Make the PUT request to update the data
      const response = await fetch(`https://backendexample.sanbercloud.com/api/mobile-apps/${id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedData),
      });
  
      if (response.ok) {
        console.log(`Data with ID ${id} updated successfully`);
        // You may want to update the state or fetch the data again after a successful update
      } else {
        console.error(`Failed to update data with ID ${id}`);
      }
    } catch (error) {
      console.error('Error updating data:', error);
    }
  };
  

  const handleDeleteData = async (id) => {
    try {
      const response = await fetch(`https://backendexample.sanbercloud.com/api/mobile-apps/${id}`, {
        method: 'DELETE',
        // Add any headers if needed
      });

      if (response.ok) {
        console.log(`Data with ID ${id} deleted successfully`);
        // You may want to update the state or fetch the data again after a successful delete
      } else {
        console.error(`Failed to delete data with ID ${id}`);
      }
    } catch (error) {
      console.error('Error deleting data:', error);
    }
  };

  // State to manage form data
  const [formData, setFormData] = useState({
    imageUrl: '',
    name: '',
    category: '',
    description: '',
    price: 0,
    rating: 0,
    releaseYear: 2009,
    size: 0,
    android: false,
    ios: false,
  });

  const handleCreateData = async (e) => {
    e.preventDefault();
  
    try {
      // Prepare the data to be sent
      const postData = {
        name: formData.name,
        description: formData.description,
        category: formData.category,
        release_year: formData.releaseYear,
        size: formData.size,
        price: formData.price,
        rating: formData.rating,
        image_url: formData.imageUrl,
        is_android_app: formData.android ? 1 : 0,
        is_ios_app: formData.ios ? 1 : 0,
      };
  
      // Send a POST request to create data
      const response = await fetch('https://backendexample.sanbercloud.com/api/mobile-apps', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(postData),
      });
  
      // Check if the request was successful
      if (response.ok) {
        // Fetch the updated data and update the state
        const fetchData = async () => {
          try {
            const response = await fetch('https://backendexample.sanbercloud.com/api/mobile-apps');
            const data = await response.json();
            setMobileApps(data);
          } catch (error) {
            console.error('Error fetching data:', error);
          }
        };
  
        // Trigger fetching updated data
        fetchData();
  
        // Reset the form data
        setFormData({
          imageUrl: '',
          name: '',
          category: '',
          description: '',
          price: 0,
          rating: 0,
          releaseYear: 2009,
          size: 0,
          android: false,
          ios: false,
        });
  
        alert('Data created successfully!');
      } else {
        // Handle errors
        console.error('Error creating data:', response.statusText);
        alert('Error creating data. Please try again.');
      }
    } catch (error) {
      console.error('Error creating data:', error);
      alert('Error creating data. Please try again.');
    }
  };
  

  return (
    <div className="container mx-auto mt-10 pr-8">
      <h1 className="text-xl font-bold mb-5">Manage Data</h1>
        <table className="border-collapse rounded-lg w-full mb-8">
          <thead>
            <tr>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Name</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Description</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Category</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Release Year</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Size</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Price</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Rating</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Image URL</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Android</th>
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">iOS</th>
              {/* Add other table headers as needed */}
              <th className="p-3 font-bold uppercase bg-purple-100 text-gray-600 hidden lg:table-cell">Actions</th>
            </tr>
          </thead>
          <tbody>
            {mobileApps.map((app) => (
              <tr key={app.id}>
                <td className="py-2 px-4 border-b">{app.name}</td>
                <td className="py-2 px-4 border-b">{app.description}</td>
                <td className="py-2 px-4 border-b">{app.category}</td>
                <td className="py-2 px-4 border-b">{app.release_year}</td>
                <td className="py-2 px-4 border-b">{app.size}</td>
                <td className="py-2 px-4 border-b">{app.price}</td>
                <td className="py-2 px-4 border-b">{app.rating}</td>
                <td className="py-2 px-4 border-b">{app.image_url}</td>
                <td className="py-2 px-4 border-b">{app.is_android_app ? 'Yes' : 'No'}</td>
                <td className="py-2 px-4 border-b">{app.is_ios_app ? 'Yes' : 'No'}</td>
                {/* Add other table data cells as needed */}
                <td>
                  <div className="inline-flex xs:mt-0">
                    <button
                      className="text-sm bg-yellow-500 hover:bg-yellow-700 text-white py-1 px-1 rounded focus:outline-none focus:shadow-outline"
                      onClick={() => handleUpdateData(app.id)}
                    >
                      Update
                    </button>
                    &nbsp; &nbsp;
                    <button
                      className="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-1 rounded focus:outline-none focus:shadow-outline"
                      onClick={() => handleDeleteData(app.id)}
                    >
                      Delete
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>


      {/* Create Data Form */}
      <div>
        <h2 className="text-lg font-bold mb-3">Create Data</h2>
        <form onSubmit={handleCreateData}>
          {/* Image URL */}
          <div className="mb-3">
            <label htmlFor="imageUrl" className="block text-sm font-medium text-gray-700">
              Image URL
            </label>
            <input
              type="text"
              id="imageUrl"
              name="imageUrl"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Name */}
          <div className="mb-3">
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              Name
            </label>
            <input
              type="text"
              id="name"
              name="name"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Category */}
          <div className="mb-3">
            <label htmlFor="category" className="block text-sm font-medium text-gray-700">
              Category
            </label>
            <input
              type="text"
              id="category"
              name="category"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Description */}
          <div className="mb-3">
            <label htmlFor="description" className="block text-sm font-medium text-gray-700">
              Description
            </label>
            <textarea
              id="description"
              name="description"
              rows="3"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            ></textarea>
          </div>

          {/* Price */}
          <div className="mb-3">
            <label htmlFor="price" className="block text-sm font-medium text-gray-700">
              Price
            </label>
            <input
              type="number"
              id="price"
              name="price"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Rating */}
          <div className="mb-3">
            <label htmlFor="rating" className="block text-sm font-medium text-gray-700">
              Rating
            </label>
            <input
              type="number"
              id="rating"
              name="rating"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Release Year */}
          <div className="mb-3">
            <label htmlFor="releaseYear" className="block text-sm font-medium text-gray-700">
              Release Year
            </label>
            <input
              type="number"
              id="releaseYear"
              name="releaseYear"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Size */}
          <div className="mb-3">
            <label htmlFor="size" className="block text-sm font-medium text-gray-700">
              Size (MB)
            </label>
            <input
              type="number"
              id="size"
              name="size"
              className="mt-1 p-2 border rounded-md w-full"
              // Add your onChange and value props as needed
            />
          </div>

          {/* Platform */}
          <div className="mb-3">
            <label className="block text-sm font-medium text-gray-700 mb-1">
              Platform
            </label>
            <div className="flex">
              <div className="mr-3">
                <label htmlFor="android" className="inline-flex items-center">
                  <input type="checkbox" id="android" name="android" className="form-checkbox h-5 w-5 text-blue-600" />
                  <span className="ml-2 text-gray-700">Android</span>
                </label>
              </div>
              <div>
                <label htmlFor="ios" className="inline-flex items-center">
                  <input type="checkbox" id="ios" name="ios" className="form-checkbox h-5 w-5 text-blue-600" />
                  <span className="ml-2 text-gray-700">iOS</span>
                </label>
              </div>
            </div>
          </div>

          {/* Submit Button */}
          <div>
            <button
              type="submit"
              className="px-4 py-2 bg-green-500 text-white rounded"
            >
              Create Data
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ManageData;
